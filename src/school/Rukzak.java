package school;

import java.util.*;

/**
 * Created by yuri on 05.01.2018.
 */
public class Rukzak {

    // Число предментов в рюкзаке
    private static int n = 5;
    // Ограничение по массе
    private static int maxMass = 10;

    static boolean ending = false;

    static List<Item> items;

    static List<Item> currentItems = new ArrayList();
    static Item current = new Item();
    static List<Item> optItems = null;
    static Item max = new Item();

    static Random rnd = new Random(1);


    public static class ItemComparator implements Comparator<Item> {
        @Override
        public int compare(Item a, Item b) {
            if (a.price / a.mass > b.price / b.mass)
                return -1;

            if (a.price / a.mass < b.price / b.mass)
                return 1;

            return 0;
        }
    }


    static void next(int i) {
        i++;
        if ((i > items.size() - 1) || (current.mass + items.get(i).mass > maxMass)) {
            if (current.price > max.price) {
                optItems = Collections.unmodifiableList(currentItems);
                max = new Item();
                max.mass = current.mass;
                max.price = current.price;
                if (current.mass == max.mass)
                    ending = true;
                return;
            }
        }

        currentItems.add(items.get(i));
        current.mass += items.get(i).mass;
        current.price += items.get(i).price;
        next(i);

        currentItems.remove(currentItems.size() - 1);
        current.mass -= items.get(i).mass;
        current.price -= items.get(i).price;
        if (ending != true)
            next(i);
    }

    public static class Item {
        public int mass;
        public int price;

        public String toString() {
            return String.format("Цена %s, Масса: %s", price, mass);
        }
    }

    public static void main(String[] args) {
        {
            items = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                Item item = new Item();
                double v = Math.random();
                double p = Math.random();
                int ves = (int) (v * 10);
                int price = (int) (p * 10);
                item.mass = ves == 0 ? ves + 1 : ves;
                item.price = price == 0 ? price + 1 : price;
                items.add(item);
            }

            Collections.sort(items, new ItemComparator());

            System.out.println("Исходные данные (отсортированы по удельной стоимости):");
            for (Item item : items) {
                System.out.println(item.toString());
            }
            System.out.println("");
            System.out.println("------------------");
            System.out.println("");
            System.out.println("Оптимальный набор туриста:");

            max.price = -1;
            next(-1);

            if (optItems != null) {
                for (Item item : optItems) {
                    System.out.println(item.toString());
                }
                System.out.println("Итого:");
                System.out.println(max);
            } else {
                System.out.println("Что-то не так...");
            }
        }
    }
}

