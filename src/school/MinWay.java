package school;

import java.util.Scanner;

/**
 * Created by yuri on 10.12.2017.
 */
public class MinWay {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        //int n = 3;
        //int[][] arrayOfInputData = {{9, 4, 3},{2, 1, 6},{0, 9, 1}};
        int[][] arrayOfInputData = new int[n][n];
        int[][] arrayOfSum = new int[n][n];
        char[][] arrayOfWay = new char[n][n];

        createArrays(n, arrayOfInputData, arrayOfWay);

        print(arrayOfInputData);

        writeSumArray(n, arrayOfInputData, arrayOfSum);

        checkMin(n, arrayOfInputData, arrayOfSum);

        way(n, arrayOfSum, arrayOfWay);

        print(arrayOfWay);
    }

    private static void createArrays(int n, int[][] arrayOfInputData, char[][] arrayOfWay) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                double randNumber = Math.random();
                arrayOfInputData[i][j] = (int) (randNumber * 10);
                arrayOfWay[i][j] = '-';
            }
        }
        arrayOfWay[n - 1][n - 1] = '#';
    }

    private static void writeSumArray(int n, int[][] sourceArray, int[][] sumArray) {
        int sum1 = 0, sum2 = 0;
        for (int i = 0; i < n; i++) {
            sum1 += sourceArray[0][i];
            sumArray[0][i] = sum1;
            sum2 += sourceArray[i][0];
            sumArray[i][0] = sum2;
        }
    }

    private static void checkMin(int n, int[][] arr, int[][] arrSum) {
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                arrSum[i][j] = Math.min(arrSum[i][j - 1], arrSum[i - 1][j]) + arr[i][j];
            }
        }
    }

    private static void way(int n, int[][] arrSum, char[][] result) {
        int x = n - 1;
        int y = n - 1;
        while (x != -1 && y != -1) {
            if (x > 0 && y > 0 && arrSum[x][y - 1] <= arrSum[x - 1][y]) {
                result[x][y - 1] = '#';
                y--;
            } else if (x > 0 && y > 0 && arrSum[x - 1][y] < arrSum[x][y - 1]) {
                result[x - 1][y] = '#';
                x--;
            } else if (x == 0) {
                result[x][y - 1] = '#';
                y--;
            } else if (y == 0) {
                result[x - 1][y] = '#';
                x--;
            }
            if (x == 0 && y == 0) {
                break;
            }
        }
    }


    private static void print(int[][] arr) {
        for (int[] anArr : arr) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(anArr[j] + " ");
            }
            System.out.println();
        }
    }

    private static void print(char[][] arr) {
        for (char[] anArr : arr) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(anArr[j] + " ");
            }
            System.out.println();
        }
    }
}


