package school;

/**
 * Created by yuri on 07.01.2018.
 */
public class R_4832 {


    public static void main(String[] args) {

/*

        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
*/

        int weights[] = {1, 2, 3, 5, 7};
        int costs[] = {1, 1, 1};
        int needed = 10;
        int res = knapsack(weights, costs, needed);
        System.out.println(res);

    }


    private static int knapsack(int weights[], int costs[], int needed) {
        int n = weights.length;
        int dp[][] = new int[needed + 1][n + 1];
        for (int j = 1; j <= n; j++) {
            for (int w = 1; w <= needed; w++) {
                if (weights[j - 1] <= w) {
                    dp[w][j] = Math.max(dp[w][j - 1], dp[w - weights[j - 1]][j - 1] + costs[j - 1]);
                } else {
                    dp[w][j] = dp[w][j - 1];
                }
            }
        }
        return dp[needed][n];
    }
}
