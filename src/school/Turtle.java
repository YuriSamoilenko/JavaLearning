package school;

import java.util.Scanner;

/**
 * Created by yuri on 09.12.2017.
 */
public class Turtle {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();


        int[][] arr = new int[m][n];

        createSourceArray(m, n, arr);

        print(m, n, arr);

        findTurtleWay(m - 1, n - 1, arr);

        print(1, 1, arr);
    }

    private static void createSourceArray(int m, int n, int[][] arr) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                double randNumber = Math.random();
                arr[i][j] = (int) (randNumber * 10);
            }
        }
    }

    private static void findTurtleWay(int m, int n, int[][] arr) {
        for (int i = m; i >= 0; i--) {
            for (int j = n; j >= 0; j--) {
                int s1 = 0;
                if (i < m) {
                    s1 = arr[i][j] + arr[i + 1][j];
                }
                int s2 = 0;
                if (j < n) {
                    s2 = arr[i][j] + (j < n ? arr[i][j + 1] : 0);
                }
                if (s1 > 0) {
                    if (s2 > 0) {
                        arr[i][j] = s1 < s2 ? s1 : s2;
                    } else {
                        arr[i][j] = s1;
                    }
                } else {
                    if (s2 > 0) {
                        arr[i][j] = s2;
                    }
                }
            }
        }
    }

    private static void print(int m, int n, int[][] arr) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
