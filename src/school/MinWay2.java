package school;

import java.util.Scanner;

/**
 * Created by yuri on 11.12.2017.
 */
public class MinWay2 {


    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = m;


        int[][] arr = new int[m][n];
        char[][] arrWay = new char[m][n];

        createSourceArray(m, n, arr, arrWay);
        print(m, n, arr);
        findTurtleWay(m - 1, n - 1, arr);
        way(m, n, arr, arrWay);
        print(arrWay);
    }

    private static void createSourceArray(int m, int n, int[][] arr, char[][] arrWay) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                double randNumber = Math.random();
                arr[i][j] = (int) (randNumber * 10);
                arrWay[i][j] = '-';
            }
        }
        arrWay[0][0] = '#';
    }

    private static void findTurtleWay(int m, int n, int[][] arr) {
        for (int i = m; i >= 0; i--) {
            for (int j = n; j >= 0; j--) {
                int s1 = 0;
                if (i < m) {
                    s1 = arr[i][j] + arr[i + 1][j];
                }
                int s2 = 0;
                if (j < n) {
                    s2 = arr[i][j] + (j < n ? arr[i][j + 1] : 0);
                }
                if (s1 > 0) {
                    if (s2 > 0) {
                        arr[i][j] = s1 < s2 ? s1 : s2;
                    } else {
                        arr[i][j] = s1;
                    }
                } else {
                    if (s2 > 0) {
                        arr[i][j] = s2;
                    }
                }
            }
        }
    }

    private static void way(int m, int n, int[][] arrSum, char[][] result) {
        int x = 0;
        int y = 0;
        while (x != m && y != n) {
            if (x < m - 1 && y < n - 1 && arrSum[x][y + 1] <= arrSum[x + 1][y]) {
                result[x][y + 1] = '#';
                y++;
            } else if (x < m - 1 && y < n - 1 && arrSum[x + 1][y] < arrSum[x][y + 1]) {
                result[x + 1][y] = '#';
                x++;
            } else if (x == m - 1) {
                result[x][y + 1] = '#';
                y++;
            } else if (y == n - 1) {
                result[x + 1][y] = '#';
                x++;
            }
            if (x == m - 1 && y == m - 1) {
                break;
            }
        }
    }

    private static void print(int m, int n, int[][] arr) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void print(char[][] arr) {
        for (char[] anArr : arr) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(anArr[j] + " ");
            }
            System.out.println();
        }
    }
}
