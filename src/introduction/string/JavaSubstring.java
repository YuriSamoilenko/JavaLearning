package introduction.string;

import java.util.Scanner;

/**
 * Created by yuri on 12.11.2017.
 * <p>
 * Given a string, , and two indices,  and , print a substring consisting of all characters in the inclusive range from  to . You'll find the String class' substring method helpful in completing this challenge.
 * <p>
 * Input Format
 * <p>
 * The first line contains a single string denoting .
 * The second line contains two space-separated integers denoting the respective values of  and .
 * <p>
 * Constraints
 * <p>
 * String  consists of English alphabetic letters (i.e., ) only.
 * Output Format
 * <p>
 * Print the substring in the inclusive range from  to .
 * <p>
 * Sample Input
 * <p>
 * Helloworld
 * 3 7
 * Sample Output
 * <p>
 * lowo
 */
public class JavaSubstring {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String S = in.next();
        int start = in.nextInt();
        int end = in.nextInt();

        if (S.length() > 0 && S.length() <= 100 && S.replaceAll("[A-Za-z]", "").length() == 0 &&
                start < end && end <= S.length() && start >= 0) {
            System.out.println(String.format("%s", S.substring(start, end)));
        }
    }
}
