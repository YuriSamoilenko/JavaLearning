package introduction.string;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by yuri on 13.11.2017.
 * <p>
 * Given a string, , matching the regular expression [A-Za-z !,?._'@]+, split the string into tokens. We define a token to be one or more consecutive English alphabetic letters. Then, print the number of tokens, followed by each token on a new line.
 * <p>
 * Note: You may find the String.split method helpful in completing this challenge.
 * <p>
 * Input Format
 * <p>
 * A single string, .
 * <p>
 * Constraints
 * <p>
 * is composed of any of the following: English alphabetic letters, blank spaces, exclamation points (!), commas (,), question marks (?), periods (.), underscores (_), apostrophes ('), and at symbols (@).
 * Output Format
 * <p>
 * On the first line, print an integer, , denoting the number of tokens in string  (they do not need to be unique). Next, print each of the  tokens on a new line in the same order as they appear in input string .
 * <p>
 * Sample Input
 * <p>
 * He is a very very good boy, isn't he?
 * Sample Output
 * <p>
 * 10
 * He
 * is
 * a
 * very
 * very
 * good
 * boy
 * isn
 * t
 * he
 */
public class JavaStringTokens {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        if (s.length() > 0 && s.length() < 4 * Math.pow(10, 5)) {
            List<String> result = Arrays.stream(s.split("\\s+|A-Za-z|\\!|\\,|\\?|\\.|\\_|\\'|\\@"))
                    .filter(s1 -> s1.length() > 0)
                    .collect(Collectors.toList());
            System.out.println(result.size());
            for (String res : result) {
                System.out.println(res);
            }
        }
        scan.close();
    }
}
