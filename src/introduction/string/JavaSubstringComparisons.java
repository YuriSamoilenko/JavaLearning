package introduction.string;

import java.util.Scanner;

/**
 * Created by yuri on 12.11.2017.
 */
public class JavaSubstringComparisons {

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";
        if (s.length() > 1 && s.length() <= 1000 && s.replaceAll("[a-zA-Z]", "").length() == 0) {
            for (int i = 0; i < s.length() - k + 1; i++) {
                String current = s.substring(i, i + k);
                if (smallest.length() == 0) {
                    smallest = current;
                }
                if (smallest.compareTo(current) > 0) {
                    smallest = current;
                }
                if (current.compareTo(largest) > 0) {
                    largest = current;
                }
            }
        }
        return smallest + "\n" + largest;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();

        System.out.println(getSmallestAndLargest(s, k));
    }
}


