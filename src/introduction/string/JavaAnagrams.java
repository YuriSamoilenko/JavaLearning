package introduction.string;

import java.util.Scanner;

/**
 * Created by yuri on 13.11.2017.
 * <p>
 * Two strings,  and , are called anagrams if they contain all the same characters in the same frequencies. For example, the anagrams of CAT are CAT, ACT, TAC, TCA, ATC, and CTA.
 * <p>
 * Complete the function in the editor. If  and  are case-insensitive anagrams, print "Anagrams"; otherwise, print "Not Anagrams" instead.
 * <p>
 * Input Format
 * <p>
 * The first line contains a string denoting .
 * The second line contains a string denoting .
 * <p>
 * Constraints
 * <p>
 * Strings  and  consist of English alphabetic characters.
 * The comparison should NOT be case sensitive.
 * Output Format
 * <p>
 * Print "Anagrams" if  and  are case-insensitive anagrams of each other; otherwise, print "Not Anagrams" instead.
 * <p>
 * Sample Input 0
 * <p>
 * anagram
 * margana
 * Sample Output 0
 * <p>
 * Anagrams
 */
public class JavaAnagrams {

    static boolean isAnagram(String a, String b) {
        a = a.toUpperCase();
        b = b.toUpperCase();
        if (a.length() != b.length()) {
            return false;
        }
        for (int i = 0; i < a.length(); i++) {
            char ch = a.charAt(i);
            if (b.indexOf(ch) > -1) {
                b = b.replaceFirst(String.valueOf(ch), "");
            }
        }
        return b.length() == 0;
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println((ret) ? "Anagrams" : "Not Anagrams");
    }
}


