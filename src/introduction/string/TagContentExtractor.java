package introduction.string;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by yuri on 18.11.2017.
 */
public class TagContentExtractor extends Throwable {

    public static final String regularExpression = "\\<\\/?(\\w+|\\d+)\\s?(\\w+)?\\>";

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile(regularExpression);

        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        while (testCases > 0) {
            String line = in.nextLine();

            System.out.println(line.replaceAll("", ""));

            testCases--;
        }
    }
}
