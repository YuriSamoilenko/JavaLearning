package introduction.string;

import java.util.Scanner;

/**
 * Created by yuri on 12.11.2017.
 */
public class JavaStringReverse {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String A = sc.next();

        int length = A.length();
        String res = "Yes";
        for (int i = 0; i < Math.floorDiv(length, 2); i++) {
            if (A.charAt(i) != A.charAt(length - 1 - i)) {
                res = "No";
                break;
            }
        }
        System.out.println(res);
    }
}
