package introduction.string;

import java.util.Scanner;

/**
 * Created by yuri on 12.11.2017.
 */
public class JavaStringsIntroduction {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String A = sc.next();
        String B = sc.next();

        System.out.println(A.length() + B.length());
        System.out.println(String.format("%s", A.compareTo(B) > 0 ? "Yes" : "No"));
        System.out.println(String.format("%s%s %s%s", A.toUpperCase().charAt(0), A.substring(1, A.length()),
                B.toUpperCase().charAt(0), B.substring(1, B.length())));
    }
}
