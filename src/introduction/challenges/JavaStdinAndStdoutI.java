package introduction.challenges;

import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * Most HackerRank challenges require you to read input from stdin (standard input) and write output to stdout (standard output).
 * <p>
 * One popular way to read input from stdin is by using the Scanner class and specifying the Input Stream as System.in. For example:
 * <p>
 * Scanner scanner = new Scanner(System.in);
 * String myString = scanner.next();
 * int myInt = scanner.nextInt();
 * scanner.close();
 * <p>
 * System.out.println("myString is: " + myString);
 * System.out.println("myInt is: " + myInt);
 * The code above creates a Scanner object named  and uses it to read a String and an int. It then closes the Scanner object because there is no more input to read, and prints to stdout using System.out.println(String). So, if our input is:
 * <p>
 * Hi 5
 * Our code will print:
 * <p>
 * myString is: Hi
 * myInt is: 5
 * Alternatively, you can use the BufferedReader class.
 * <p>
 * Task
 * In this challenge, you must read  integers from stdin and then print them to stdout. Each integer must be printed on a new line. To make the problem a little easier, a portion of the code is provided for you in the editor below.
 * <p>
 * Input Format
 * <p>
 * There are  lines of input, and each line contains a single integer.
 * <p>
 * Sample Input
 * <p>
 * 42
 * 100
 * 125
 * Sample Output
 * <p>
 * 42
 * 100
 * 125
 */
public class JavaStdinAndStdoutI {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int a2 = scan.nextInt();
        int a3 = scan.nextInt();

        System.out.println(a);
        System.out.println(a2);
        System.out.println(a3);
    }
}
