package introduction.challenges;

import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * "In computing, End Of File (commonly abbreviated EOF) is a condition in a computer operating system where no more data can be read from a data source." — (Wikipedia: End-of-file)
 * The challenge here is to read  lines of input until you reach EOF, then number and print all  lines of content.
 * <p>
 * Hint: Java's Scanner.hasNext() method is helpful for this problem.
 * <p>
 * Input Format
 * <p>
 * Read some unknown  lines of input from stdin(System.in) until you reach EOF; each line of input contains a non-empty String.
 * <p>
 * Output Format
 * <p>
 * For each line, print the line number, followed by a single space, and then the line content received as input.
 * <p>
 * Sample Input
 * <p>
 * Hello world
 * I am a file
 * Read me until end-of-file.
 * Sample Output
 * <p>
 * 1 Hello world
 * 2 I am a file
 * 3 Read me until end-of-file.
 */
public class JavaEndOfFile {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = 1;

        while(sc.hasNext()) {
            String s = sc.nextLine();
            System.out.printf("%d. %s%n",count,s);
            count++;
        }
    }

}
