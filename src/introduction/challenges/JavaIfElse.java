package introduction.challenges;

import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * Task
 * Given an integer, , perform the following conditional actions:
 * <p>
 * In this challenge, we test your knowledge of using if-else conditional statements to automate decision-making processes. An if-else statement has the following logical flow:
 * <p>
 * Wikipedia if-else flow chart
 * <p>
 * Source: Wikipedia
 * <p>
 * Task
 * Given an integer, , perform the following conditional actions:
 * <p>
 * If  is odd, print Weird
 * If  is even and in the inclusive range of  to , print Not Weird
 * If  is even and in the inclusive range of  to , print Weird
 * If  is even and greater than , print Not Weird
 * Complete the stub code provided in your editor to print whether or not  is weird.
 * <p>
 * Input Format
 * <p>
 * A single line containing a positive integer, .
 * <p>
 * Constraints\
 * 1<=N<=100
 * <p>
 * Output Format
 * <p>
 * Print Weird if the number is weird; otherwise, print Not Weird.
 * <p>
 * Sample Input 0
 * <p>
 * 3
 * Sample Output 0
 * <p>
 * Weird
 * Sample Input 1
 * <p>
 * 24
 * Sample Output 1
 * <p>
 * Not Weird
 * Explanation
 * <p>
 * Sample Case 0:
 * is odd and odd numbers are weird, so we print Weird.
 * <p>
 * Sample Case 1:
 * and  is even, so it isn't weird. Thus, we print Not Weird.
 */
public class JavaIfElse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String ans = "";
        if (n % 2 == 1) {
            ans = "Weird";
        } else {
            if (n >= 2 && n <= 5) {
                ans = "Not Weird";
            } else if (n >= 6 && n <= 20) {
                ans = "Weird";
            } else if (n > 20) {
                ans = "Not Weird";
            }
        }
        System.out.println(ans);
    }
}
