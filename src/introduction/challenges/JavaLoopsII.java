package introduction.challenges;

import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * We use the integers , , and  to create the following series:
 * <p>
 * You are given  queries in the form of , , and . For each query, print the series corresponding to the given , , and  values as a single line of  space-separated integers.
 * <p>
 * Input Format
 * <p>
 * The first line contains an integer, , denoting the number of queries.
 * Each line  of the  subsequent lines contains three space-separated integers describing the respective , , and  values for that query.
 * <p>
 * Constraints
 * <p>
 * Output Format
 * <p>
 * For each query, print the corresponding series on a new line. Each series must be printed in order as a single line of  space-separated integers.
 * <p>
 * Sample Input
 * <p>
 * 2
 * 0 2 10
 * 5 3 5
 * Sample Output
 * <p>
 * 2 6 14 30 62 126 254 510 1022 2046
 * 8 14 26 50 98
 * Explanation
 * <p>
 * We have two queries:
 * <p>
 * We use , , and  to produce some series :
 * <p>
 * ... and so on.
 * <p>
 * Once we hit , we print the first ten terms as a single line of space-separated integers.
 * <p>
 * We use , , and  to produce some series :
 * <p>
 * We then print each element of our series as a single line of space-separated values.
 */
public class JavaLoopsII {

    public static void main(String[] argh) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        if (q >= 0 && q <= 500) {
            for (int i = 0; i < q; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                int n = in.nextInt();
                if (a >= 0 && a <= 500 && b >= 0 && b <= 500 && n >= 1 && n <= 15) {
                    for (int idx = 0; idx < n; idx++) {
                        a += Math.pow(2, idx) * b;
                        System.out.printf("%d ", a);
                    }
                    System.out.printf("%n");
                }
            }
        }
        in.close();
    }

}
