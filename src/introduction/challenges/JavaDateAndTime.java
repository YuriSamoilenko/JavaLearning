package introduction.challenges;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * The Calendar class is an abstract class that provides methods for converting between a specific instant in time and a set of calendar fields such as YEAR, MONTH, DAY_OF_MONTH, HOUR, and so on, and for manipulating the calendar fields, such as getting the date of the next week.
 * <p>
 * You are given a date. To simplify your task, we have provided a portion of the code in the editor. You just need to write the method, , which returns the day on that date.
 * <p>
 * For example, if you are given the date , the method should return  as the day on that date.
 * <p>
 * Input Format
 * <p>
 * A single line of input containing the space separated month, day and year, respectively, in   format.
 * <p>
 * Constraints
 * <p>
 * Output Format
 * <p>
 * Output the correct day in capital letters.
 * <p>
 * Sample Input
 * <p>
 * 08 05 2015
 * Sample Output
 * <p>
 * WEDNESDAY
 * Explanation
 * <p>
 * The day on August th  was WEDNESDAY.
 */
public class JavaDateAndTime {

/*
    public static String getDay(String day, String month, String year) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
        calendar.add(Calendar.MONTH, Integer.valueOf(month) - 1);
        calendar.add(Calendar.YEAR, Integer.valueOf(year));
        return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH);
    }
*/

    public static String getDay(String day, String month, String year) {
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(Locale.US);
        String weekdays[] = dateFormatSymbols.getWeekdays();
        Calendar cal = new GregorianCalendar(Locale.US);
        cal.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
        return weekdays[cal.get(Calendar.DAY_OF_WEEK)].toUpperCase();
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String month = in.next();
        String day = in.next();
        String year = in.next();
        System.out.println(getDay(day, month, year));
    }
}
