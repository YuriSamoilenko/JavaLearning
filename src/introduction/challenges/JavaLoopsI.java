package introduction.challenges;

import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * Objective
 * In this challenge, we're going to use loops to help us do some simple math.
 * <p>
 * Task
 * Given an integer, , print its first  multiples. Each multiple  (where ) should be printed on a new line in the form: N x i = result.
 * <p>
 * Input Format
 * <p>
 * A single integer, .
 * <p>
 * Constraints
 * <p>
 * 2<= N <= 20
 * <p>
 * Output Format
 * <p>
 * Print  lines of output; each line  (where ) contains the  of  in the form:
 * N x i = result.
 * <p>
 * Sample Input
 * <p>
 * 2
 * Sample Output
 * <p>
 * 2 x 1 = 2
 * 2 x 2 = 4
 * 2 x 3 = 6
 * 2 x 4 = 8
 * 2 x 5 = 10
 * 2 x 6 = 12
 * 2 x 7 = 14
 * 2 x 8 = 16
 * 2 x 9 = 18
 * 2 x 10 = 20
 */
public class JavaLoopsI {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n >= 1 && n <= 20) {
            for (int i = 1; i <= 10; i++) {
                System.out.format("%d x %d = %d%n", n, i, n * i);
            }
        }
    }
}
