package introduction.challenges;

import java.security.Permission;
import java.util.Scanner;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * You are given an integer , you have to convert it into a string.
 * <p>
 * Please complete the partially completed code in the editor. If your code successfully converts  into a string  the code will print "Good job". Otherwise it will print "Wrong answer".
 * <p>
 * can range between  to  inclusive.
 * <p>
 * Sample Input 0
 * <p>
 * 100
 * Sample Output 0
 * <p>
 * Good job
 */
public class JavaIntToString {

    public static void main(String[] args) {

        DoNotTerminate.forbidExit();

        try {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            in.close();
            if (n < -100 || n > 100) {
                throw new DoNotTerminate.ExitTrappedException();
            }
            String s = String.valueOf(n);
            if (n == Integer.parseInt(s)) {
                System.out.println("Good job");
            } else {
                System.out.println("Wrong answer.");
            }
        } catch (DoNotTerminate.ExitTrappedException e) {
            System.out.println("Unsuccessful Termination!!");
        }
    }
}

//The following class will prevent you from terminating the code using exit(0)!
class DoNotTerminate {

    public static class ExitTrappedException extends SecurityException {

        private static final long serialVersionUID = 1;
    }

    public static void forbidExit() {
        final SecurityManager securityManager = new SecurityManager() {
            @Override
            public void checkPermission(Permission permission) {
                if (permission.getName().contains("exitVM")) {
                    throw new ExitTrappedException();
                }
            }
        };
        System.setSecurityManager(securityManager);
    }
}


