package introduction.challenges;

/**
 * Created by yuri on 11.11.2017.
 * <p>
 * Welcome to the world of Java! In this challenge, we practice printing to stdout.
 * <p>
 * The code stubs in your editor declare a Solution class and a main method. Complete the main method by copying the two lines of code below and pasting them inside the body of your main method.
 * <p>
 * System.out.println("Hello, World.");
 * System.out.println("Hello, Java.");
 * Input Format
 * <p>
 * There is no input for this challenge.
 * <p>
 * Output Format
 * <p>
 * You must print two lines of output:
 * <p>
 * Print Hello, World. on the first line.
 * Print Hello, Java. on the second line.
 * Sample Output
 * <p>
 * Hello, World.
 * Hello, Java.
 */
public class WelcomeToJava {

    public static void main(String[] args) {
        System.out.println("Hello, World.");
        System.out.println("Hello, Java.");
    }
}
